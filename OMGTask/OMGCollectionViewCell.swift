//
//  OMGCollectionViewCell.swift
//  OMGTask
//
//  Created by Hakob Nalbandyan on 17.03.24.
//

import UIKit

class OMGCollectionViewCell: UICollectionViewCell {
    @IBOutlet var title: UILabel!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touche begin")
        UIView.animate(withDuration: 1) {
            self.contentView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 1) {
            self.contentView.transform = .identity
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 1) {
            self.contentView.transform = .identity
        }
    }
    
    func configureCell(with title: String) {
        self.title.text = title
    }
}
