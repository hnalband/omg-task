//
//  OMGTableViewCell.swift
//  OMGTask
//
//  Created by Hakob Nalbandyan on 17.03.24.
//

import UIKit

class OMGTableViewCell: UITableViewCell {
    @IBOutlet var rowCollection: UICollectionView!
    
    var rowData = [String]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(with rowData: [String]) {
        self.rowData = rowData
        self.rowCollection.reloadData()
    }
    
    func updateCollectionCell(at indexPath: IndexPath) {
        self.rowCollection.reloadItems(at: [indexPath])
    }

}

extension OMGTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return rowData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let omgCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "OMGCollectionViewCell", for: indexPath) as! OMGCollectionViewCell
        
        if indexPath.row < self.rowData.count {
            let title = self.rowData[indexPath.row]
            omgCollectionViewCell.configureCell(with: title)
        } else {
            omgCollectionViewCell.configureCell(with: "???")
        }
        return omgCollectionViewCell
    }
}
