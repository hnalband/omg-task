//
//  OMGViewController.swift
//  OMGTask
//
//  Created by Hakob Nalbandyan on 17.03.24.
//

import UIKit

class OMGViewController: UIViewController {
    @IBOutlet var omgTableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    private var updateTimer: Timer?
    private var currentPage = 1
    private var pageLimit = 100
    private let numberOfRows = 100 + Int(arc4random())
    private var isLoadingMoreItems = false
    
    private var dataSource = [[String]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    
        self.generateData()
        self.activityIndicator.isHidden = true
        
        self.omgTableView.reloadData()
        self.startTimer()
    }
    
    private func loadData() {
        self.activityIndicator.isHidden = false
        self.updateTimer?.invalidate()
        self.updateTimer = nil
        
        self.isLoadingMoreItems = true
        
        let nextRow = self.dataSource.count
        let remainingRows = self.numberOfRows - (self.currentPage * self.pageLimit)
        if remainingRows > self.pageLimit {
            self.currentPage += 1
            self.generateData()
        } else {
            self.generateData(numberOfRows: remainingRows)
        }

        var insertingIndexPaths: [IndexPath] = []
        for row in nextRow..<self.dataSource.count {
            insertingIndexPaths.append(IndexPath(item: row, section: 0))
        }
        
        print(Thread.current)
        DispatchQueue.main.async {
            self.omgTableView.performBatchUpdates({
                self.omgTableView.insertRows(at: insertingIndexPaths,
                                             with: .automatic)
            }) { finished in
                if finished {
                    self.isLoadingMoreItems = false
                    self.startTimer()
                    self.activityIndicator.isHidden = true
                }
            }
        }
    }
    
    private func startTimer() {
        self.updateTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
            if let omgVisibleCells = self.omgTableView.visibleCells as? [OMGTableViewCell],
               let omgVisibleCellsIndexes = self.omgTableView.indexPathsForVisibleRows {
                let firstVisibleRow = omgVisibleCellsIndexes[0].row
                    for (index, omgVisibleCell) in omgVisibleCells.enumerated() {
                        let cellIndex = Int.random(in: 0..<self.dataSource[firstVisibleRow + index].count)
                        if omgVisibleCell.rowCollection.indexPathsForVisibleItems.contains(where: { $0.row == cellIndex
                        }) {
                            let newTitle = "\(Int.random(in: 0..<1000))"
                            self.dataSource[firstVisibleRow + index][cellIndex] = newTitle

                            omgVisibleCell.rowData = self.dataSource[firstVisibleRow + index]
                            omgVisibleCell.rowCollection.reloadItems(at: [IndexPath(row: cellIndex, section: 0)])
                        }
                    }
                }
        }
    }
    
    private func generateData(numberOfRows: Int = 100) {
        for _ in 0..<numberOfRows {
            let numberOfCells = Int.random(in: 10..<20)
            var rowCells = [String]()
            for _ in 0..<numberOfCells {
                let title = "\(Int.random(in: 0..<1000))"
                rowCells.append(title)
            }
            dataSource.append(rowCells)
        }
    }

}

extension OMGViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let omgTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OMGTableViewCell", for: indexPath) as! OMGTableViewCell
        
        let rowData = self.dataSource[indexPath.row]
        omgTableViewCell.configureCell(with: rowData)
        
        return omgTableViewCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (dataSource.count - 1) &&
            !self.isLoadingMoreItems &&
            (currentPage * pageLimit) < numberOfRows {
            self.loadData()
        }
    }
}
